﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Set GameManager instance
    public static GameManager instance;

    // Call to game object enemy
    public GameObject enemy;

    // Call to game object meteor
    public GameObject meteor;

    // Placements for the spawn points
    public Vector3 spawnValues;

    // Variable on chances for enemy ship to spawn
    public int enemySpawnChance;

    // variable on chances for meteor to spawn
    public int meteorSpawnChance;

    // Variable for limit of enemies
    public int totalEnemies;

    // Variable for spawn wait of enemies
    public float spawnWait;

    // Variable for waiting to spawn at start
    public float startWait;

    // Variable to limit the wave amount
    public float waveWait;

    // Variable for player score
    public int score;

    // Variable for player lives
    public int playerLives;

   // Function to ensure there is not more than one GameManager
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        else
        {
            Destroy(this.gameObject);
        }
    }

    // At start run SpawnWaves function
    void Start()
    {
        StartCoroutine(SpawnWaves());
    }

    //
    IEnumerator SpawnWaves()
    {
        // Wait by value of variable startWait
        yield return new WaitForSeconds(startWait);

        
        while (true)
        {
            for (int i = 0; i < totalEnemies; i++)
            {
                // Place spawn position at spawn values with random x values(-, +)
                Vector3 spawnPos = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRot = Quaternion.identity;

                // Create Random generator variable
                int numGen = Random.Range(0, totalEnemies);

                // Random Generator to determine to spawn meteors or enemies
                if (numGen < totalEnemies/2)
                {
                    Instantiate(meteor, spawnPos, spawnRot);
                }

                else
                {
                    Instantiate(enemy, spawnPos, spawnRot);
                }

                // After wait by value of spawnWait
                yield return new WaitForSeconds(spawnWait);
            }
        }

        // After wait by value of waveWaite
        yield return new WaitForSeconds(waveWait);
    }

}
