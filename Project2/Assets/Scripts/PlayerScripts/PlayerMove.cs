﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    // Public variable for player movement speed
    public float speed;

    // Public variable for player boost
    private float boost;

    // Set Transform to variable tf
    private Transform tf;

    // Variable for rotation speed of player
    public float rotateSpeed;

	// Use this for initialization
	void Start () {
        // Get transform component
        tf = GetComponent<Transform>();
	}

    // Update is called once per frame
    void Update()
    {
        // axisX is used for controller of player
        float axisX = Input.GetAxis("Horizontal");

        // Moves sprite forward and backward
        float axisY = Input.GetAxis("Vertical");

        // Allows sprite to move at speed input
        transform.Translate(new Vector2(0, axisY) * speed * Time.deltaTime);

        // Rotate left
        if (Input.GetKey(KeyCode.A))
        {
            tf.Rotate(0, 0, rotateSpeed);
        }

        // Rotate right
        if (Input.GetKey(KeyCode.D))
        {
            tf.Rotate(0, 0, -rotateSpeed);
        }
    
        // Use Player boost, speed value * 2
        if (Input.GetKey(KeyCode.E))
        {
           transform.Translate(new Vector2(0, axisY) * speed * Time.deltaTime * 2); ;
        }
    }
}
