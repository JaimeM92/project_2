﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    // Public GameObject to place bullet for shooting
    public GameObject bullet;

    // Public vairable to set delay for when bullet can be shot
    public float fireDelay;

    // Public variable to set time when player can fire
    public float bulletTimer;
	
	// Update is called once per frame
	void Update ()
    {
        // Value of timer is set to value of delay
        bulletTimer -= Time.deltaTime;

        // if SPACE is keyed, shoot bullet
        if(Input.GetKeyDown(KeyCode.Space) && bulletTimer <= 0)
        {
            // timer is set to value of delay
            bulletTimer = fireDelay;

            // Sets shot in front of player
            Vector3 offset = transform.rotation * new Vector3(0, 1.5f, 0);

            // Calls bullet into scene
            Instantiate(bullet, transform.position, transform.rotation);
        }
	}
}
