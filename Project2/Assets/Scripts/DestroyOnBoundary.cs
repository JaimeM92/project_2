﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnBoundary : MonoBehaviour
{
    // Function to destroy game object that exits play area
    void OnTriggerExit2D(Collider2D other)
    {
        Destroy(other.gameObject);
    }

}
