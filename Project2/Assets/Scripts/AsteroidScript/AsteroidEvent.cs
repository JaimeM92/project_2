﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidEvent : MonoBehaviour {

    // Function to destroy object that collides with asteroid
    void OnTriggerEnter2D(Collider2D other)
    {
        // If Asteroid is == to player, Destroy both game objects
        if (other.gameObject.tag == "Player") 
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
