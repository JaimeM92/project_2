﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidMove : MonoBehaviour
{
    // Public variable to change the speed of the asteroid
    public float astSpeed;
    // Variable to obtain location of target GameObject (player)
    public Transform target;

    // Update is called once per frame
    void Update ()
    {
        // Set the direction object is facing
        Vector3 directionToLook = target.position - transform.position;

        // Transform. up is set to direction of target object
        transform.up = directionToLook;

        // Set position to transform position
        Vector3 pos = transform.position;

        // Move asteroid in direction 
        Vector3 velocity = new Vector3(0, 0, astSpeed * Time.deltaTime);

        // Position adds rotation * velocity (movement)
        pos += transform.rotation * velocity;

        // transform postion is set to position
        transform.position = pos;
    }
}
