﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBullet : MonoBehaviour
{
    // Variable to place timer on bullet lifetime
    public float bulletTimer;

    void Update()
    {
        // Bullet time line subtracts in real time
        bulletTimer -= Time.deltaTime;

        // Timer <= 0, destroy bullet
        if(bulletTimer <= 0)
        {
            Destroy(gameObject);
        }
    }

}
