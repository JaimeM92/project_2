﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEvent : MonoBehaviour
{
    // Function to destroy object that bullet collides with
    void OnTriggerEnter2D(Collider2D other)
    {
        // if bullet is == to either game object, Destroy bullet w/ game object
        if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "Meteor")
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
