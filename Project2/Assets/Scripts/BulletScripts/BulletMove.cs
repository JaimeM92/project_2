﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour
{
    // Public variable for speed of bullet
    public float bulletSpeed;
	
	// Update is called once per frame
	void Update ()
    {
        // Set postion of bullet
        Vector3 pos = transform.position;

        // Move bullet in direction by speed
        Vector3 velocity = new Vector3(0, bulletSpeed * Time.deltaTime, 0);

        // Position adds rotation * velocity (movement)
        pos += transform.rotation * velocity;

        // Trnasform position is set to position
        transform.position = pos;

    }
}
