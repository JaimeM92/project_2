﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEvent : MonoBehaviour
{
    // Function to destroy object that collides with enemy
    void OnTriggerEnter2D(Collider2D other)
    {
        // If Enemy is == to player, Destroy both game objects
        if (other.gameObject.tag == "Player")
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
