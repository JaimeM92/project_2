﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    // Public Variable for speed of enemy ship
    public float enemySpeed;

    // Public variable for rotation speed of enemy ship
    public float eRotateSpeed;

    // Set transform target for gameObject
   public Transform target;
	
	// Update is called once per frame
	void Update ()
    {
        // Variable set to movement of enemy
        float step = enemySpeed * Time.deltaTime;

        // Set the direction object is facing
        Vector3 directionToLook = target.position - transform.position;

        // transform. up is set to direction of target object
        transform.up = directionToLook;

        // Move game object towards target object
        transform.position = Vector3.MoveTowards(transform.position, target.position, step);

        // Rotate game object towards target object
        transform.rotation = Quaternion.RotateTowards(transform.rotation, target.rotation, step);
	}
    
   


}
